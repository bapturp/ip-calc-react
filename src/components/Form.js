const Form = (props) => {
    const handleChange = (event) => {
        props.setCidr(event.target.value)
    }

    return (
       <form>
           <input
                id="ipv4Form"
                type="text"
                placeholder="192.168.0.1/24"
                title="ipv4"
                autoComplete="off"
                value={props.cidr}
                onChange={handleChange}>
           </input>
       </form>
    )
}

export default Form
