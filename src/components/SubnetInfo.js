import validateCidr from '../utils/validateCidr'

const SubnetInfo = (props) => {

    const ip_info = validateCidr(props.cidr)
    
    if (ip_info) {
        return (
            <div>
                <p>Address is {ip_info.addr}</p>
                <p>Subnet mask is {ip_info.mask}</p>
            </div>
        )
    } else {
        return (
            <div>
                <p>Enter CIDR (CIDR notation, such as: 192.168.0.1/24)</p>
            </div>
        )
    }
}

export default SubnetInfo
