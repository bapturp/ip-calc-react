import { useState } from "react"

import Header from './components/Header';
import Form from './components/Form';
import SubnetInfo from './components/SubnetInfo'


const App = () => {
  const [cidr, setCidr] = useState("")  

  return (
    <div className="App">
      <Header 
        title={"IP Calc"}
      />
      <Form 
        setCidr={setCidr} 
        cidr={cidr}
      />
      <SubnetInfo
        cidr={cidr}/>
    </div>
  );
}

export default App;
