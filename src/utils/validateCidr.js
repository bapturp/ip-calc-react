const validateCidr = (cidr) => {

    // catch enaught case so that we can proceed to convert to binary
    // 001.001.001.001 instead of 1.1.1.1 is valid
    // TODO: implement full rfc where hexadecimal is also allow like 192.a8.0.1
    // TODO: implement ipv6
    const toPointedMask = (mask_cidr) => {
        const mask_list = [
            "0.0.0.0", // /0
            "128.0.0.0", // /1
            "192.0.0.0", // /2
            "224.0.0.0", // /3
            "240.0.0.0", // /4
            "248.0.0.0", // /5
            "252.0.0.0", // /6
            "254.0.0.0", // /7
            "255.0.0.0", // 8
            "255.128.0.0", // /9
            "255.192.0.0", // /10
            "255.224.0.0", // /11
            "255.240.0.0", // /12
            "255.248.0.0", // /13
            "255.252.0.0", // /14
            "255.254.0.0", // /15
            "255.255.0.0", // 16
            "255.255.128.0", // /17
            "255.255.192.0", // /18
            "255.255.224.0", // /19
            "255.255.240.0", // /20 - 4096 @ip
            "255.255.248.0", // 21 - 2048 @ip
            "255.255.252.0", // 22 - 1024 @ip
            "255.255.254.0", // /23 - 512 @ip
            "255.255.255.0", // 24 - 256 @ip
            "255.255.255.128", //25 - 128 @ip
            "255.255.255.192", // /26 - 64 @ip
            "255.255.255.224", // /27 - 32 @ip
            "255.255.255.240", // /28 - 16 @ip
            "255.255.255.248", // /29 - 8 @ip
            "255.255.255.252", // /30 - 4 @ip
            "255.255.255.254", // /31 - 2 @ip
            "255.255.255.255" // /32 - 1 @ip
        ]

        return mask_list[mask_cidr]
    }

    const isValidIp = (addr) => {
        const reIpv4 = /^(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/
        if (reIpv4.test(addr)) {
            return true
        } else {
            return false
        }
    }

    const isValidMask = (mask) => {
        if (mask >=0 && mask <= 32) {
            return true
        } else {
            return false
        }
    }

    const cidr_splited = cidr.split("/").filter(i => i)

    const [addr, mask_cidr] = cidr_splited

    if (isValidIp(addr) && isValidMask(mask_cidr)) {
        return ({
            addr: addr,
            mask_cidr: mask_cidr,
            mask: toPointedMask(mask_cidr)
        })
    } else {
        return false
    }
}

export default validateCidr
